workspace {

    model {

        profesor = person "Profesor" "Personal docente que pueden ingresar las actividades generales de sus estudiantes."
        padre = person "Padre" "Seguimiento de tareas, Seguimiento de asistencias, Seguimiento de calificaciones."
        personal = person "Personal" "Personal administrativo de la escuela y administradores del sistema."

        sistemaEstudiantil = softwareSystem "Sistema Estudiantil" "Aplicación integral de gestión de estudiantes como servicio."{
            aplicacionWeb = container "Aplicacion Web" "Ofrece el contenido estático y la aplicación de página única del sistema estudiantil."
            spa = container "Aplicacion de una sola pagina" "Proporciona la funcionalidad total del sistema a los usuarios a través de su navegador web."

            movil = container "Aplicacion Movil" "Proporciona un conjunto limitado de funciones para realizar desde el teléfono."
            api = container "Microservicios API" "Contiene toda la funcionalidad del sistema estudiantil llamado a través de una API JSON/HTTPS."{
                tags "Software System"
                controladorAutenticacion = component "Controlador de autentificacion" "Autenticacion Nodejs"
                controladorSeguridad = component "Controlador de seguridad" "Proporciona funcionalidad acorde a los privilegios y roles del usuario"
                controladorEmail = component "Controlador de Email" "Construye el correo acorde a la necesidad para enviarlo a traves de un servicios o puente de salida(host,paas)"
                controladorEstudiante = component "Controlador de Estudiantes" "Proporciona los servicios para administrar a la entidad estudiante"
                controladorReporte = component "Controlador generacion de reporte" "Proporciona los servicios para generar reportes"
                controladorActividades = component "Controlador de actividades" "Proporciona los servicios para administrar las activiades dentro del sistema"
                controladorForo = component "Controlador de Foros" "Proporciona los servicios para administrar los foros y sus accesos"

            }
            baseDatos = container "Base de Datos" "Almacena toda la información de los registros y actividades de todos los usuarios, tanto padres, profesores y personal." "OracleSQL" "Database"

        }
        saas = softwareSystem "Saas" "Almacena toda la información que ingresen los usuarios referente a los estudiantes."{
            tags ExternalSystem
        }
        sistemaEmail = softwareSystem "Sistema de Email." "Envía correos de notificaciones de la aplicación hacia los usuarios."{
            tags ExternalSystem
        }

        profesor -> sistemaEstudiantil "Ingresa de tareas."
        padre -> sistemaEstudiantil "Verifica calificaciones."
        personal -> sistemaEstudiantil "Administra Tareas."

        sistemaEstudiantil -> saas "Obtiene información del estudiante, como datos de calificaciones, tareas, etc."
        sistemaEstudiantil -> sistemaEmail "Envío correo electrónico a usuario."

        profesor -> aplicacionWeb "Ingresa a www.escuelafacil.com.ec Usando https."
        padre -> aplicacionWeb "Ingresa a www.escuelafacil.com.ec Usando https."
        personal -> aplicacionWeb "Ingresa a www.escuelafacil.com.ec Usando https."

        profesor -> spa "Ingreso de tareas, justificaciones, calificaciones y foros."
        padre -> spa "Seguimiento de tareas, calificaciones, asistencia y foros."
        personal -> spa "Administración de tareas, perfiles, permisos e informes."
        aplicacionWeb -> spa "Entrega al navegador web del usuario."
        spa -> api "Realiza llamadas API JSON /HTTPS."
        movil -> api "Realiza llamadas API JSON /HTTPS."
        api -> baseDatos "Leer y escribir(SQL/TCP)."

        api -> sistemaEmail "Envío correo electrónico a usuario."
        api -> saas "Llamadas API."

        spa -> controladorAutenticacion "Realiza llamadas rest"
        movil -> controladorAutenticacion "Realiza llamadas rest"
        spa -> controladorEstudiante "Realiza llamadas rest"
        movil -> controladorEstudiante "Realiza llamadas rest"

        controladorAutenticacion -> controladorSeguridad "Conexion rest valida usuarios y privilegios"
        controladorSeguridad -> controladorEmail "Conexion rest envia email o notificaciones"

        controladorEstudiante -> controladorReporte "Conexion rest genera reportes"
        controladorEstudiante -> controladorActividades "Conexion rest administrador de actividades"
        controladorEstudiante -> controladorForo "Conexion rest administrador de Foros"

        controladorActividades -> controladorEmail "Conexion rest envia email o notificaciones"
        controladorSeguridad -> baseDatos "Lee y escribe SQL"
        controladorEmail -> sistemaEmail "Conexion rest envia json para envio de correo"
    }

    views {
        systemContext sistemaEstudiantil "SystemContext" {
            include *
            autoLayout
        }

        container sistemaEstudiantil "Container" {
            include *
            autoLayout
        }

        component api "Component" {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }

            element "Container" {
                background #1168bd
                color #ffffff
            }

            element "ExternalSystem" {
                background #808080
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }

            element "Component" {
                background #85bbf0
                color #000000
            }

            element "Database" {
                shape Cylinder
            }
        }
    }
}