# Base de Datos Encriptada

## Status

Propuesto

## Contexto

Se tiene un antecedente de robo de información de un competidor del mercado por ello hemos adaptado nuevas técnicas de seguridad de datos, como encriptación de la información y cifrado, para garantizar la confiabilidad de los datos del usuario.
La seguridad de los datos es una preocupación fundamental en el desarrollo de aplicaciones, especialmente cuando se maneja información sensible de los usuarios. En este sentido, la encriptación de la base de datos es una medida fundamental para proteger la confidencialidad y privacidad de la información almacenada.

## Problema

Teniendo en cuenta los antecedentes, el problema al que se enfrenta es la necesidad de garantizar la seguridad de la información almacenada en la base de datos de una aplicación. Sin una adecuada protección, los datos podrían estar expuestos a accesos no autorizados y ser vulnerables a posibles ataques o violaciones de seguridad. Esto podría resultar en la pérdida de datos sensibles, la violación de la privacidad de los usuarios y el incumplimiento de regulaciones y normativas relacionadas con la protección de datos.

## Decisiones

Se toma como decisión que las bases de datos de todos los ambientes van a estar encriptadas a nivel de archivo general de base de datos. 
Se debe investigar y evaluar diferentes algoritmos de encriptación, como AES, y RSA, para determinar cuál es el más adecuado para proteger la base de datos. 
Es necesario establecer prácticas adecuadas para generar, almacenar y proteger las claves de encriptación.
Se debe establecer un sistema de gestión de acceso seguro para la base de datos encriptada.
Se debe decidir el protocolo de comunicación que se utilizará para asegurar la transmisión de datos entre la aplicación y la base de datos.  


## Consecuencias

- Generación de mayor consumo de CPU en los servidores de base de datos.
- Los tiempos de respuesta de la base de datos pueden aumentar.
- En tiempos de despliegue aumentará la complejidad.
- La encriptación de la base de datos garantiza la confidencialidad de los datos almacenados.
- La base de datos encriptada dificulta enormemente los intentos de acceso no autorizados a la información almacenada.
- Generar confianza en los usuarios de la aplicación. 

## Fecha Última Actualización
11-Julio-2023