# Seguridad del Sistema

## Status

Propuesto

## Contexto

Estamos viviendo una era de transformación digital en la cual varias de las actividades que se realizan de forma tradicional se están digitalizando, como consecuencia la información de las empresas al igual que sus usuarios podrían ser vulneradas y utilizada con fines delictivos. Uno de nuestros objetivos es proporcionar a nuestros clientes la seguridad y confianza que nuestros productos utilizan métodos de cifrados de información con la adopción de técnicas, reglamentos y estándares internacionales para el uso y protección de sus datos.

Un punto de importancia en nuestra labor es aprender del mercado, revisar los casos en los cuales los ciberdelincuentes han corrompido los sistemas y que tecnicas han utilizado en sus ataques, esto nos ha ayudado ha reforzar y actualizar puntos de importancia en nuestros sistemas. La mejoras contempladas en la propuesta es utilización JWT(Json Web Token), Protección de los campos en los cuales el usuario inigresa texto, cifrado en la base de datos, a su vez mejorando su estructura y componentes de seguridad.

## Problema

La seguridad del sistema puede verse afectada por varios factores los mismos que hemos corregido pero seguimos mejorando. El ataque más reciente fue hacia uno de nuestros competidores donde se vieron compretidos los datos de sus usuarios, se ha analizado las posibles causas del ataque a su vez se ha estimado un tiempo prudente para tomar acciones correctivas y preventivas de los equipos y sistemas.

## Decisiones

Realizamos un análisis de los requisitos de seguridad desde lo más básicos hasta las más robustaz aplicadas en compañias internacionales con su trayectoria establecida. Esto implica comprender los posibles costos que se podrian generar, como ejemplo tenemos los mecanismos de cifrada mientras más robusto y seguro sea mayor capacidad de computo se requerira, considerando el número de transacciones, usuarios, etc. Al estar utilizando un desarrollo modular a traves del uso de microservicios nos favorece en realizar cambios sin afectar el uso completo de la aplicación, podremos realizar nuestras pruebas en entornos aislados corrigiendo los fallos y poder aplicarlos de manera eficaz, estas acciones las aplicariamos en nuestros entornos de frontend al ingresar texto o valor, banckend que tipo de información se esta procesando con una previa revisión y base de datos.

## Consecuencias

- Pueden existir implicaciones como un mayor esfuerzo y tiempo de desarrollo para implementar pruebas.
- La implementación de estas decisiones contribuirá a mejorar la confiabilidad del software.
- Podria implicar un gasto de recursos de hardaware y software.

## Fecha Última Actualización

12-Julio-2023
