# Diseño responsive

## Status

Propuesto

## Contexto

Pese a ser un sistema web la mayoria de padres de familia por cuestiones de tiempo y trabajo usan dispositivos moviles.

## Decisiones

Se toma como decisión que el sistema debe ser usado en multiples pantallas y dispositivos por lo cual debe mantener su correcto funcionamiento y muestra datos para los distintos usuarios.

## Consecuencias

- Tiempo empleado al realizar el diseño en diferentes dispositivos.
- Aumento de demanda en peticiones al servidor.
- Aumento de tiempo al desarrollar pruebas para el frontend.

## Fecha Última Actualización
24-Junio-2023