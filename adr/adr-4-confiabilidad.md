# Confiabilidad del software

## Status

Propuesto

## Contexto

En este adr nos enfocaremos en abordar el tema de la confiabilidad del software. La confiabilidad es un aspecto crítico para garantizar el correcto funcionamiento del sistema y la satisfacción de los usuarios. Nuestro objetivo es establecer decisiones y medidas que mejoren la confiabilidad del software durante el proceso de desarrollo.

Como se mencionó en el apartado de base de datos, se tiene un antecedente de robo de información de un competidor del mercado por ello hemos adaptado nuevas técnicas de seguridad, ya que la aplicación brindará el mayor porcentaje de confiabilidad a sus usuarios.
Debemos asegurar y garantizar alta disponibilidad la mayor cantidad de tiempo.

## Problema

La confiabilidad del software puede verse afectada por diversos factores, como errores de diseño, falta de pruebas adecuadas, insuficiente manejo de errores y fallos en la gestión de la recuperación ante situaciones inesperadas. Es necesario abordar estos problemas desde la etapa de diseño de la arquitectura para asegurar que el software sea confiable y cumpla con las expectativas de los usuarios.

## Decisiones

Realizaremos un análisis exhaustivo de los requisitos de confiabilidad del software. Esto implica identificar y comprender los atributos de confiabilidad específicos que deben cumplirse, como disponibilidad, rendimiento, tolerancia a fallos y capacidad de recuperación.
Aplicaremos un enfoque de diseño modular y de componentes independientes para facilitar la identificación y resolución de problemas de confiabilidad. Esta modularidad nos permitirá aislar y corregir fallos en componentes específicos sin afectar el funcionamiento general del sistema.
Realizaremos pruebas exhaustivas en diferentes etapas del desarrollo para evaluar la confiabilidad del software.
Implementaremos mecanismos adecuados de gestión de errores y excepciones. 


## Consecuencias

- La implementación de estas decisiones contribuirá a mejorar la confiabilidad del software.
- Pueden existir implicaciones como un mayor esfuerzo y tiempo de desarrollo para implementar pruebas y mecanismos de recuperación.

## Fecha Última Actualización
08-Julio-2023